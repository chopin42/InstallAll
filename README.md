## Todo

* Making more tests
* Creating a template to create new ones
* Support older architectures
* Adding more apps (see bellow)

## Todo apps
Please post your favorite apps in the issues to get them added here.

* MarkText
* Ghostwriter
* Firefox
* Chromium
* Brave
* Tilix
* Thunderbird
* Minecraft
* Libreoffice
* openoffice
* OBS
* Python


## NodeJS (v12.18.3)
Should work on every Linux distro. Tested on Ubuntu and Debian based distributions. Not compatible with 32bit.


```bash
curl https://gitea.com/chopin42/InstallAll/raw/branch/master/node.sh | sh
```

## Discord (v0.0.11)
Should work on every Linux distro. Tested on Debian and Ubuntu based distributions. Not compatible with 32bit.

```bash
curl https://gitea.com/chopin42/InstallAll/raw/branch/master/discord.sh | sh
```

## Python (any versions)
Should work on every Linux distro. 

```bash
curl https://gitea.com/chopin42/InstallAll/raw/branch/master/python.sh | sh
```

## Catalina macOS theme for Linux (latest)
Should work on every Linux distro. 

```bash
curl https://gitea.com/chopin42/InstallAll/raw/branch/master/catalina-theme.sh | sh
```
