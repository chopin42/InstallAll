#!/bin/sh

OS=$(uname)
VERSION="v12.18.3"
NPM="6.14.6"

# Checking if node is already installed
if [ $(which node) ]; then
	echo "Node is installed. Checking version"
	if [ "$(node -v)" = "$VERSION" ]; then
		echo "Your node installation is up to date"
		exit 1
	else
		echo "Your node installation is not $VERSION, please remove it before proceeding further"
		exit 1
	fi
else
	echo "Node is not installed, installing..."
fi

if [ "$OS" = "darwin" ]; then
	echo "Mac systems are not supported for now"

elif [ "$OS" = "Linux" ]; then
	echo "You are using Linux" $(uname -r)
	if [ -f /etc/redhat-release ]; then
		echo "You are using a RedHat system"
		sudo rpm install -y wget

	elif [ -f /etc/debian_version ]; then
		echo "You are using a Debian system"
		sudo apt install -y wget
	elif [ -f /etc/SuSE-release ]; then
		echo "You are using a SUSE system, but your system isn't supported yet."
	else
		echo "Your system is unknown"
	fi
fi

# Downloading and moving files
wget https://nodejs.org/dist/$VERSION/node-$VERSION-linux-x64.tar.xz
tar xf node-$VERSION-linux-x64.tar.xz
sudo cp node-$VERSION-linux-x64/ /opt/node/

# Cleaning up
rm -r node-$VERSION-linux-x64/
rm node-$VERSION-linux-x64.tar.xz

# Adding stuff to profile
echo 'export NODEJS_HOME=/opt/node/bin' >> ~/.profile
echo 'export PATH=$NODEJS_HOME:$PATH' >> ~/.profile
. ~/.profile

# Adding stuff to bashrc
echo 'export NODEJS_HOME=/opt/node/bin' >> ~/.bashrc
echo 'export PATH=$NODEJS_HOME:$PATH' >> ~/.bashrc
source ~/.bashrc

# Test node and npm
echo "If the two commands bellow renders the correct version, then everything is fine"
NODEV=$(node -v)
NPMV=$(npm -v)

# to avoid needing to run "sudo"
mkdir "${HOME}/.npm-packages"
npm config set prefix "${HOME}/.npm-packages"
echo 'NPM_PACKAGES="${HOME}/.npm-packages"' >> ~/.bashrc
echo 'export PATH="$PATH:$NPM_PACKAGES/bin"' >> ~/.bashrc
echo 'export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"' >> ~/.bashrc

# Check the install
if [ "$NODEV" = "$VERSION" ]; then
	echo "The installation of Node $NODEV is successful"
fi

if [ "$NPMV" = "$NPM" ]; then
	echo "The installation of NPM $NPMV is successful"
fi

