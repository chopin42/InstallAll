#!/bin/sh
echo "What version do you want? (example: '3.6.0'"
read VERSION
read VERSION

OS=$(uname)

if [ "$OS" = "darwin" ]; then
	if [ $(which wget) ]; then
		echo "This installation should proceed without any issues"
	else
		echo "This installation requires wget"
		exit 1
	fi

elif [ "$OS" = "Linux" ]; then
	if [ $(which wget) ]; then
		echo "This installation should proceed without any issues"
	else
		if [ $(which apt) ]; then
			sudo apt install -y wget
		elif [ $(which rpm) ]; then
			sudo rpm install -y wget
		fi
	fi
else
	echo "Your system is unknown, this installation requires Wget"
fi

# Download python
wget https://www.python.org/ftp/python/$VERSION/Python-$VERSION.tar.xz
tar xf Python-$VERSION.tar.xz
cd Python-$VERSION/

# Make the install
./configure
make
make test
sudo make altinstall

# Confirm the install
echo "Python $VERSION should be installed properly"

