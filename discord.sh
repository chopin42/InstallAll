#!/bin/sh

OS=$(uname)
VERSION="0.0.11"
PATH="/usr/share/discord"

# Check if it's already installed
if [ $(which discord) ]; then
	echo "Discord is already installed"
	exit 1
fi

# Find the system
if [ "$OS" = "darwin" ]; then
	echo "The installation of Discord on macOS is not supported yet"
	exit 1
elif [ "$OS" = "Linux" ]; then
	if [ -f /etc/debian_version ]; then
		wget https://dl.discordapp.net/apps/linux/$VERSION/discord-$VERSION.deb
		sudo dpkg -i discord-$VERSION.deb
		sudo apt install -f -y
	else
		wget https://dl.discordapp.net/apps/linux/$VERSION/discord-$VERSION.tar.gz
		tar xf discord-$VERSION.tar.gz
		sudo mv Discord $PATH
		sudo mv $PATH /usr/share/applications/

		# Cleaning up
		rm discord-$VERSION.tar.gz
	fi
fi




