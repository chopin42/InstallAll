#/bin/sh

OS=$(uname)

if [ "$OS" = "darwin" ]; then
	echo "Are you really trying to install a macOS theme on macoS?"
	exit 1
elif [ "$OS" = "Linux" ]; then
	if [ $(which git) ]; then
		echo "This installation should proceed without any issues"
	else
		if [ $(which apt) ]; then
			sudo apt install -y git
		elif [ $(which apt-get) ]; then
			sudo apt-get install -y git
		elif [ $(which rpm) ]; then
			sudo rpm install -y git
		else
			echo "Please install git before continuing"
		fi
	fi
fi

# Create the directory
mkdir ~/.themes
cd ~/.themes

echo "Choose an option, type 'dark' or 'light'"
read THEME

if [ "$THEME" = "dark" ]; then
	echo "Installing dark theme"
	git clone https://github.com/B00merang-Project/macOS-Catalina-Dark
elif [ "$THEME" = "light" ]; then
	echo "Installing light theme"
	git clone https://github.com/B00merang-Project/macOS-Catalina
else
	echo "$THEME is an invalid option. Only 'dark' and 'light' are correct."
	exit 1
fi

echo "Everything should be installed properly, go into your theme manager to enable the theme."
